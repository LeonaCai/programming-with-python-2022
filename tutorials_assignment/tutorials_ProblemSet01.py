#!/usr/bin/env python
# coding: utf-8

# In[10]:


# 1
# 30! mod 59

def num(x):
    result = 0
    for i in (1,x):
        result = i*1
    return(result)

a = num(30)%59
print(a)


# In[12]:


# 2
# 2^100 mod 7

print((2**100)%7)


# In[26]:


# 3
# the integer part of the division of the integer obtained by concatenating 99 9s, divided by 25#
# str to int
result = (int("9"*99))//25
print(result)



# In[32]:

#4
# How many bits are needed to represent 33^33 ?
result = len(bin(33**33))-2

print(result)

## Vu Lam's answer print( (33  ** 33).bit_length() )
print( (33  ** 33).bit_length() )


# In[35]:


# How many digits does the decimal representation of 33^33 have ?

result = len(str(33**33))
print(result)


# In[61]:


# Find lowest and highest value of the absolute values of the numbers −10, 5, 20, −35

lst = [-10, 5 , 20 , -35]

print(" the lowest value is:",min(lst),","," the highest value is:", max(lst))


# In[62]:


# Calculate the area of a circle with radius 3.
r = 3
s = (3**2)*3.14

print( "the area of the circle is:", s)


# In[64]:


# Find the number of ±1 walks of 20 steps that start and end in 0, 
# i.e., where in each step, your “position” increases or decreases by 1.
## a better wording from Vu Lam is "How many different way of walking 20 steps of ±1 from 0 that will ends up at 0". Hope it is clearer for you."
# print(20//2)




# In[68]:


# The size of the human genome is approx. n = 3 100 000 000. What is log2(n) ?
import numpy as np
a = np.log2(3100000000)
print("log2(n) = " + str(a))

